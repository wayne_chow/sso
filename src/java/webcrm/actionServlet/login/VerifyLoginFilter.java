/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrm.actionServlet.login;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import webcrm.bean.ui.LanguageElement;
import webcrm.log.*;
import webcrm.util.SSOProperties;

/**
 *
 * @author eric0-kwong
 */
public class VerifyLoginFilter implements Filter {

    private List<String> allowList;
    private List<String> blockFolderList;
    private List<String> blockDocumentList;
    private String redirectTo;
    private static Logger accessLog;
    private static Logger generalLog;
    private static Logger fullAccessLog;

    private static enum STATUS {

        ALLOW, BLOCK, ERROR;

        private STATUS() {
        }
    }
    private boolean isForceHTTPS = false;
    private int httpsPort = 8181;

    public void init(FilterConfig config)
            throws ServletException {
        try {
            if (!SSOProperties.isBuilded()) {
                SSOProperties ssoProperties = new SSOProperties();

                String propertiesPath = config.getServletContext().getRealPath(File.separator);
                String propertiesFile = config.getServletContext().getInitParameter("SystemPropertiesFile").replaceAll("/", Matcher.quoteReplacement(File.separator));

                ssoProperties.buildProperties(propertiesPath + propertiesFile);
            }
        } catch (Exception ex) {
            System.out.println("Init WSMSystemProperties Fail");
            ex.printStackTrace();
        }
        initLog(config);

        accessLog.info("Filter Init Start");


        initAllowList(config);

//        this.redirectTo = (config.getServletContext().getContextPath() + config.getInitParameter("redirectTo"));
        this.redirectTo = ("/SSO" + config.getInitParameter("redirectTo"));


        initBlockPageList(config);

        initProtocol();
    }

    private void initLog(FilterConfig config) {
        String path = config.getServletContext().getRealPath(File.separator);
        String log4jpropfile = path + SSOProperties.getProperty("LOG.PROPERTIES.PATH").replaceAll("/", Matcher.quoteReplacement(File.separator));
        String generalLogFile = path + SSOProperties.getProperty("LOG.GENERALLOG.PATH").replaceAll("/", Matcher.quoteReplacement(File.separator));
        String accessLogFile = path + SSOProperties.getProperty("LOG.ACCESSLOG.PATH").replaceAll("/", Matcher.quoteReplacement(File.separator));
        String fullAccessLogFile = path + SSOProperties.getProperty("LOG.FULLACCESSLOG.PATH").replaceAll("/", Matcher.quoteReplacement(File.separator));

        accessLog = AccessLog.CreateInstance(log4jpropfile, accessLogFile);
        fullAccessLog = FullAccessLog.CreateInstance(log4jpropfile, fullAccessLogFile);
        generalLog = Log.CreateInstance(log4jpropfile, generalLogFile);

        accessLog.info("Access Log Start");
        generalLog.info("General Log Start");
    }

    private void initAllowList(FilterConfig config) {
        this.allowList = null;

        String strAllow = config.getInitParameter("allowPage");
        if (!strAllow.equals("")) {
            this.allowList = new ArrayList();
            String[] strAllowList = strAllow.split(",");

            String trimAllowPage = null;
            for (String allowPage : strAllowList) {
                if (allowPage != null) {
                    trimAllowPage = allowPage.trim();
                    if (!trimAllowPage.equals("")) {
                        this.allowList.add(trimAllowPage);
                    }
                }
            }
            Collections.sort(this.allowList);
        }
        int numOfPage;
        if (this.allowList != null) {
            numOfPage = 0;
            accessLog.info("The following page allow browse without login:");
            for (String allowPage : this.allowList) {
                numOfPage++;
                accessLog.info(numOfPage + ". " + allowPage);
            }
        } else {
            accessLog.info("No Allow Page");
        }
    }

    private void initBlockPageList(FilterConfig config) {
        String strBlockPage = config.getInitParameter("blockPage");


        this.blockFolderList = null;
        this.blockDocumentList = null;
        int i;
        if (!strBlockPage.equals("")) {
            String[] strBlockPageArray = strBlockPage.split(",");

            String blockType = null;
            String blockName = null;
            String[] tempArray = null;
            for (String blockPage : strBlockPageArray) {
                if (blockPage != null) {
                    tempArray = blockPage.split(":");

                    blockType = tempArray[0].trim();
                    blockName = tempArray[1].trim();
                    if (blockType.equalsIgnoreCase("folder")) {
                        if (this.blockFolderList == null) {
                            this.blockFolderList = new ArrayList();
                        }
                        this.blockFolderList.add(blockName);
                    } else if (blockType.equalsIgnoreCase("document")) {
                        if (this.blockDocumentList == null) {
                            this.blockDocumentList = new ArrayList();
                        }
                        this.blockDocumentList.add(blockName);
                    }
                }
            }
            if ((this.blockFolderList != null) && (this.blockDocumentList != null)) {
                i = 1;

                accessLog.info("The following folder/document are block browse direct in URL:");
                if (this.blockFolderList != null) {
                    Collections.sort(this.blockFolderList);
                    for (String folderName : this.blockFolderList) {
                        accessLog.info(i + ". Folder Name   : " + folderName);
                        i++;
                    }
                }
                if (this.blockDocumentList != null) {
                    Collections.sort(this.blockDocumentList);
                    for (String documentName : this.blockDocumentList) {
                        accessLog.info(i + ". Document Name : " + documentName);
                        i++;
                    }
                }
            }
        }
    }

    private void initProtocol() {
        String strIsHTTPS = SSOProperties.getProperty("SSO.WEB.FORCEHTTPS");
        if ((strIsHTTPS != null) && (strIsHTTPS.equalsIgnoreCase("true"))) {
            this.isForceHTTPS = true;


            this.httpsPort = Integer.parseInt(SSOProperties.getProperty("SSO.WEB.HTTPSPORT"));
        }
        if (this.isForceHTTPS) {
            accessLog.info("Force HTTPS : " + this.isForceHTTPS + " - redirect to port : " + this.httpsPort);
        } else {
            accessLog.info("Force HTTPS : " + this.isForceHTTPS);
        }
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        boolean isSessionTimeout = false;
        boolean isTargetPageInBlockList = false;
        StringBuilder logString = new StringBuilder();

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        request.setCharacterEncoding("UTF-8");

        STATUS status = null;

        String currentURL = request.getRequestURI();
        String targetPage = "";
        if (request.getContextPath().length() < currentURL.length()) {
            targetPage = currentURL.substring(request.getContextPath().length() + 1);
        }
        String queryString = request.getQueryString();
        if ((queryString != null) && (!queryString.equals(""))) {
            queryString = "?" + queryString;
        } else {
            queryString = "";
        }
        if (this.blockFolderList != null) {
            for (String blockfolder : this.blockFolderList) {
                if (currentURL.matches(blockfolder)) {
                    isTargetPageInBlockList = true;
                    break;
                }
            }
        }
        if (this.blockDocumentList != null) {
            for (String blockDocument : this.blockDocumentList) {
                if (targetPage.matches(blockDocument)) {
                    isTargetPageInBlockList = true;
                    break;
                }
            }
        }
        if ((this.isForceHTTPS) && (!request.isSecure())) {
            response.sendRedirect("https://" + request.getServerName() + ":" + this.httpsPort + currentURL);
            return;
        }
        HttpSession session = request.getSession(true);

        logString.append("SessionId :" + session.getId() + ", ");
        logString.append("RequestIP :" + request.getRemoteAddr() + ", ");
        logString.append("RequestPort :" + request.getRemotePort() + ", ");
        logString.append("RequestURI : \"" + currentURL + "\"");


        MDC.put("sessionId", session.getId());


        String newSessionId = session.getId();
        String oldSessionId = "";

        String language = "";

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName().equals("JSESSIONID")) {
                    oldSessionId = cookies[i].getValue();
                } else if (cookies[i].getName().equals("COOKIE_LANGUAGE")) {
                    language = cookies[i].getValue();
                }
            }
        }
//System.out.println("oldSessionId: " + oldSessionId + ", newSessionId: " + newSessionId);
        if ((!oldSessionId.isEmpty()) && (!oldSessionId.equals(newSessionId)) && (!targetPage.equals("SignIn"))) {
            isSessionTimeout = true;


            HashMap<String, String> hashmap = new HashMap();
            hashmap.put("error.sessiontimeout", "error.sessiontimeout");
            session.setAttribute("errorAccess", hashmap);
        }
        session.setAttribute("timeoutRequestLink", currentURL + queryString);

        Locale locale = (Locale) session.getAttribute("language");
        if (locale == null) {
            if ((language != null) && (language.length() > 0)) {
                session.setAttribute("language", LanguageElement.getLocale(language));
            } else {
                session.setAttribute("language", Locale.SIMPLIFIED_CHINESE);
            }
        }
        if (targetPage.equals("VerifySession")) {
            status = STATUS.ALLOW;
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/plain;charset=UTF-8");
            PrintWriter writer = response.getWriter();
            if (isSessionTimeout) {
                accessLog.info("[" + status + "]" + logString.toString() + ", Verify Session: Timeout");
            } else {
                accessLog.info("[" + status + "]" + logString.toString() + ", Verify Session: Valid");
            }
            writer.println(!isSessionTimeout);
            writer.flush();
//        }else if (targetPage.equals("GetSessionId")) {
//            status = STATUS.ALLOW;
//            response.setCharacterEncoding("UTF-8");
//            response.setContentType("text/plain;charset=UTF-8");
//            String clientOrigin = request.getHeader("origin");
//            if(clientOrigin.contains("www.i-services.asia")
//                     || clientOrigin.contains("localhost")){//for testing
//                response.setHeader("Access-Control-Allow-Origin", clientOrigin);
////                response.setHeader("Access-Control-Allow-Credentials", "true");
//            }
//
//            PrintWriter writer = response.getWriter();
//            accessLog.info("[" + status + "]" + logString.toString() + ", Get Session Id: " + newSessionId);
//            writer.print(newSessionId);
//            writer.flush();
        } else if (isSessionTimeout) {
            status = STATUS.BLOCK;


            String loginPage = request.getContextPath();


            accessLog.info("[" + status + "][Timeout] " + logString.toString() + ", " + " Redirect to : " + loginPage);

            response.sendRedirect(loginPage);
        } else if (targetPage.equals("SignIn")) {
            status = STATUS.ALLOW;

            String clientOrigin = request.getHeader("origin");
            if(clientOrigin.contains("www.i-services.asia")
                     || clientOrigin.contains("localhost")){//for testing
                response.setHeader("Access-Control-Allow-Origin", clientOrigin);
//                response.setHeader("Access-Control-Allow-Credentials", "true");
            }

            accessLog.info("[" + status + "]" + " " + logString.toString() + " - re-alive session");
        } else if (isTargetPageInBlockList) {
            status = STATUS.BLOCK;

            accessLog.info("[" + status + "]" + "[BlockPage]" + " " + logString.toString() + ", " + " Redirect to :" + this.redirectTo);

            response.sendRedirect(this.redirectTo);
        } else if ((targetPage.startsWith("web")) && (this.allowList != null) && (!this.allowList.contains(targetPage))) {
            status = STATUS.BLOCK;

            accessLog.info("[" + status + "]" + logString.toString() + ", " + " Redirect to : " + this.redirectTo);

            response.sendRedirect(this.redirectTo);
        } else {
            status = STATUS.ALLOW;


            accessLog.info("[" + status + "]" + " " + logString.toString());
            try {
                chain.doFilter(request, response);
            } catch (Exception ex) {
                status = STATUS.ERROR;
                generalLog.error("[" + status + "]" + " " + ex + "." + " RequestURI : \"" + currentURL + "\"", ex);
            }
        }
        printFullAccessLog(request.getRemoteAddr(), request.getRemotePort(), status, currentURL, targetPage, request.getHeader("Referer"), queryString, request.getParameterMap());


        MDC.remove("sessionId");
    }

    private void printFullAccessLog(String ip, int port, STATUS status, String currentURL, String targetPage, String referer, String queryString, Map<String, String[]> parameterMap) {
        StringBuilder fullAccessLogString = new StringBuilder();

        fullAccessLogString.append(ip).append("|");
        fullAccessLogString.append(port).append("|");

        fullAccessLogString.append(status).append("|");

        fullAccessLogString.append(currentURL).append("|");
        fullAccessLogString.append(targetPage).append("|");

        fullAccessLogString.append(referer).append("|");

        fullAccessLogString.append(queryString).append("|");
        if (parameterMap != null) {
            Iterator it = parameterMap.entrySet().iterator();
            Map.Entry pairs = null;
            String key = null;
            String[] values = null;

            int i = 0;
            while (it.hasNext()) {
                pairs = (Map.Entry) it.next();
                key = (String) pairs.getKey();
                values = (String[]) pairs.getValue();
                if (i != 0) {
                    fullAccessLogString.append(";");
                }
                if (!key.equals("password")) {
                    fullAccessLogString.append(key + ":" + Arrays.toString(values));
                } else {
                    fullAccessLogString.append(key + ":" + "[***]");
                }
                i++;
            }
        }
        fullAccessLog.debug(fullAccessLogString);
    }
}

