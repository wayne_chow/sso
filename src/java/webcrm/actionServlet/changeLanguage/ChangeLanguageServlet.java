/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.actionServlet.changeLanguage;

import java.io.IOException;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import webcrm.bean.ui.LanguageElement;

/**
 *
 * @author eric0-kwong
 */
public class ChangeLanguageServlet extends HttpServlet  {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req,res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String language = req.getParameter("language");



        HttpSession session = req.getSession();

        Locale changeLocale = LanguageElement.getLocale(language);


        session.setAttribute("language", changeLocale);

        res.setContentType("text/html;charset=UTF-8");
    }
}
