/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.actionServlet.forgetPassword;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.mail.Address;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import webcrm.log.Log;
import webcrm.util.SSOProperties;
import javax.mail.internet.InternetAddress;
import webcrm.bean.InstrumentedHashMap;
import webcrm.util.ConvertFormat;
import webcrm.util.net.mail.MailServer;
import webcrm.util.net.mail.MailServer.ConnectionType;

/**
 *
 * @author eric0-kwong
 */
public class ForgetPasswordServlet extends HttpServlet {
    
    Address[] opnAdmins = null;

    @Override
    public void init() throws ServletException {
        super.init();

        try{
            opnAdmins = new Address[]{new InternetAddress("opnadmin@wealthbridgeasia.com")};
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
    }
    
    

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req, res);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Logger log = Log.getInstance();

        try {

            req.setCharacterEncoding("UTF-8");
            res.setContentType("text/html;charset=UTF-8");

            //get Session
            HttpSession session = req.getSession();

            String adminReceiver = SSOProperties.getProperty("EMAIL.ADMIN.RECEIVER");
            String smtpUser = SSOProperties.getProperty("EMAIL.SERVER.USER");
            String smtpPassword = ConvertFormat.toString(SSOProperties.getProperty("EMAIL.SERVER.PASSWORD"));
            String sender = SSOProperties.getProperty("EMAIL.SERVER.SENDER");
            Locale locale = (Locale) session.getAttribute("language");

            //get ResourceBundle from UserInfo bean
            ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

            String clientOrigin = req.getHeader("origin");
            log.info("ForgetPasswordServlet - clientOrigin: " + clientOrigin + ", QueryString: " + req.getQueryString());
            if(clientOrigin.contains("www.i-services.asia")
                     || clientOrigin.contains("localhost")){//for testing
                res.setHeader("Access-Control-Allow-Origin", clientOrigin);
                res.setHeader("Access-Control-Allow-Credentials", "true");
            }

            String authCode = (String) session.getAttribute("ForgetPasswordAuthCode");

            if(authCode == null){
                String requestAuthTime = req.getParameter("requestAuthTime");
                if(requestAuthTime != null && !requestAuthTime.isEmpty()){
                    InstrumentedHashMap<String, String> authCodeHashMap = InstrumentedHashMap.getAuthCodeHashMap();
                    authCode = authCodeHashMap.remove(requestAuthTime);
                }
            }

            String code     = req.getParameter("authCode");
            String userId   = req.getParameter("userid");

            JSONObject returnJSONObject = new JSONObject();

            //if the authCode not equal, not process anything
            if(authCode != null && authCode.equals(code)){
                returnJSONObject.put("authCode", true);

                boolean resetPasswordMailSent = false;

                if(userId != null && !userId.isEmpty()) {
                    resetPasswordMailSent = true;
                    //send email
                    String subject = null;
                    String content = null;
                    subject = bundle.getString("forgetPassword.email.subject");
                    content = bundle.getString("forgetPassword.email.content");

                    content = MessageFormat.format(
                                    content,
                                    userId
                                );

                    MailServer mailServer = new MailServer(ConnectionType.TLS,
                            SSOProperties.getProperty("EMAIL.SERVER.HOST"),
                            Integer.parseInt(SSOProperties.getProperty("EMAIL.SERVER.PORT")),
                            new InternetAddress(sender, sender),
                            smtpUser, smtpPassword);

                    mailServer.sendHTMLMessage(adminReceiver,
                            subject,
                            "UTF-8",
                            content);
                    log.info("ForgetPasswordServlet - send mail - subject: " + subject + " content: " + content);

                } else {
                    resetPasswordMailSent = false;
                    log.info("ForgetPasswordServlet - user id not input");
                }

                returnJSONObject.put("isSuccess", resetPasswordMailSent);

                if(resetPasswordMailSent){
//                    returnJSONObject.put("message", MessageFormat.format(bundle.getString("forgetPasswordPage.caption.successMessage"), "<b>" + email + "</b>", "<b>" + email + "</b>"));
                    returnJSONObject.put("message", bundle.getString("forgetPasswordPage.caption.successMessage"));
                }else{
                    returnJSONObject.put("message", bundle.getString("forgetPasswordPage.caption.failMessage"));
                }

            }else{
                log.info("ForgetPasswordServlet - auth code error. session auth code: " + authCode + ", input auth code: " + code);
                returnJSONObject.put("authCode", false);
                returnJSONObject.put("message", bundle.getString("forgetPasswordPage.caption.authCodeErrorMessage"));
            }

            session.removeAttribute("ForgetPasswordAuthCode");

            PrintWriter writer = res.getWriter();
            writer.println(returnJSONObject);
            writer.flush();

        } catch (Exception ex) {
            log.error("Exception:" + ex, ex);
        }
    }

}
