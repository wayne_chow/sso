/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.actionServlet.forgetPassword;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import webcrm.bean.InstrumentedHashMap;
import webcrm.log.Log;
import webcrm.util.image.AuthenticationCode;
import webcrm.util.image.WiggleLen;

/**
 *
 * @author eric0-kwong
 */
public class AuthenticationCodeServlet extends HttpServlet{

    private final static int WIDTH = 700;
    private final static int HEIGHT = 200;
    private final static int FONTSIZE = 130;
    private String FONTNAME = "Times New Roman";
    private Color BGColor   = Color.WHITE;
    private Color FontColor = Color.BLACK;
    private final static int NUMBER_OF_CODE = 6;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req,res);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        
        Logger log = Log.getInstance();

        try {
            req.setCharacterEncoding("UTF-8");
            res.setContentType("image/jpeg");

            String clientOrigin = req.getHeader("origin");
            if(clientOrigin != null && (clientOrigin.contains("www.i-services.asia")
                     || clientOrigin.contains("localhost"))){//for testing
                res.setHeader("Access-Control-Allow-Origin", clientOrigin);
//                res.setHeader("Access-Control-Allow-Credentials", "true");
            }
            
            //get Session
            HttpSession session = req.getSession();

            AuthenticationCode authenticationCode = new AuthenticationCode(WIDTH, HEIGHT, new Font(FONTNAME, Font.BOLD, FONTSIZE));
            authenticationCode.setBackground(BGColor);
            //authenticationCode.setFontColor(FontColor);
            //random Color
            authenticationCode.setFontColor(null);

            String authCode = authenticationCode.randomCode(NUMBER_OF_CODE);

            //set authCode to session
            InstrumentedHashMap<String, String> authCodeHashMap = InstrumentedHashMap.getAuthCodeHashMap();
            authCodeHashMap.put(req.getQueryString(), authCode);
            session.setAttribute("ForgetPasswordAuthCode", authCode);

            BufferedImage textImage = authenticationCode.toImage(authCode);

            //use wiggle len to move the image
            WiggleLen wiggleLen = new WiggleLen((Image) textImage, BGColor.getRGB());

            //random move the image
            wiggleLen.move();

            Image image = wiggleLen.getImage();
            //Image image = textImage;

            // create BufferedImages from Image
            BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);

            // get BufferedImage's graphics context
            Graphics2D graphics2D = bufferedImage.createGraphics();
            graphics2D.drawImage(image, null, null);

            OutputStream out = res.getOutputStream();

            //use to encode the Image to JPEG
            ImageIO.write(bufferedImage, "jpg", out);
            out.close();

        }catch (Exception ex){
            log.error(ex, ex);
        }
    }

}
