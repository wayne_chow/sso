/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.log;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author eric0-kwong
 */
public class FullAccessLog{
    
    private static Logger Log = null;
    private static String loggerName;
    private static String propertyPathName;
    private static String propertyPathValue;


    public static Logger CreateInstance(String log4jPropertiesPath, String log4jLogPath){
        loggerName       = "fullAccessLog";
        propertyPathName = "fullAccessLogFileName";
        
        propertyPathValue = log4jLogPath;
        System.setProperty(propertyPathName, propertyPathValue);
        PropertyConfigurator.configure(log4jPropertiesPath);

        Log = org.apache.log4j.Logger.getLogger(loggerName);

        return Log;
    }


    public static Logger getInstance(){
        return Log;
    }

    public static void close(){
        if(Log != null){
            Log.getAppender("fullAccessLogFile").close();
        }
    }

}
