/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author eric0-kwong
 */
public class SSOProperties {

    private static String propertiesFile = null;
    private static Properties properties = null;

    public void buildProperties(String propertiesFile) throws FileNotFoundException, IOException{
        InputStream inputStream = new FileInputStream(propertiesFile);
        properties  = new Properties();
        properties.load(inputStream);
    }

    public static boolean isBuilded(){
        if(properties == null)
            return false;
        return true;
    }

    public static String getProperty(String key){
        if(properties == null)
            return null;

        return properties.getProperty(key);
    }

}
