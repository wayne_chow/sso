/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.util.image;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 *
 * @author eric0-kwong
 */
public class AuthenticationCode {

    public static Color RANDOM;
    
    //protected final static String CODE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    protected final static String CODE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private int width  = 200;
    private int height = 80;

    private Color background = Color.WHITE;
    private Color fontColor  = null;

    private int     FONTSIZE = 30;
    private String  FONTNAME = "Times New Roman";
    private Font font = new Font(FONTNAME, Font.BOLD, FONTSIZE);
    
    public AuthenticationCode(){

    }

    public AuthenticationCode(int width, int height, Font font){
        this.width  = width;
        this.height = height;
        this.font   = font;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Color getBackground() {
        return background;
    }

    public void setBackground(Color background) {
        this.background = background;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }

    public String randomCode(int length) {
        String tempCode = "";
        for (int i = 0; i < length; i++) {
            tempCode += CODE.charAt((int) (Math.random() * CODE.length()));
        }

        return tempCode;
    }

    public BufferedImage toImage(String text) {

        //System.out.println("text = " + text);

        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                
        if(fontColor != null){
            
            Graphics2D g2 = img.createGraphics();
            g2.setBackground(background);
            g2.clearRect(0, 0, width, height);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            //set Font
            g2.setFont(font);

            g2.setColor(fontColor);

            TextLayout tl = new TextLayout(text, font, g2.getFontRenderContext());
            Rectangle2D r = tl.getBounds();

            // center the text
            tl.draw(g2, (float) ((width - r.getWidth()) / 2),
                    (float) (((height - r.getHeight()) / 2) + r.getHeight()));
            g2.dispose();
        }else{

            int space = 15;

            //set background
            Graphics2D g2 = img.createGraphics();
            g2.setBackground(background);
            g2.clearRect(0, 0, width, height);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            //set Font
            g2.setFont(font);
            g2.setColor(fontColor);
            g2.dispose();

            //random color
            char textChar[] = text.toCharArray();
            int eachWidth = (width - (space * 2))/textChar.length;
            int wordSpace = space;
            for(int i = 0; i < textChar.length; i++){
                
                g2 = img.createGraphics();
                g2.setBackground(background);
                g2.clearRect((space + eachWidth * i), 0, eachWidth, height);
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                //set Font
                g2.setFont(font);

                g2.setColor(getRandomColor());

                TextLayout tl = new TextLayout(String.valueOf(textChar[i]), font, g2.getFontRenderContext());
                Rectangle2D r = tl.getBounds();

                
                tl.draw(g2, (float) wordSpace,
                        (float) (((height - r.getHeight()) / 2) + r.getHeight()));
                g2.dispose();
                //System.out.println("textChar[i] = " + textChar[i] + " " + eachWidth + " " + g2.getColor() + " " + g2.getBackground() + " " + (eachWidth * i));
                //wordSpace += r.getWidth() + 18;
                wordSpace += r.getWidth() + (font.getSize()/6);
            }
        }



        return img;
    }
    
    private Color getRandomColor(){
        Random rand = new Random();
        
        // Java 'Color' class takes 3 floats, from 0 to 1.
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
/*
        // Will produce a random colour with more red in it (usually "pink-ish")
        float r = rand.nextFloat();
        float g = rand.nextFloat(0.5);
        float b = rand.nextFloat(0.5);

        // Will produce only bright / light colours
        float r = (float) (rand.nextFloat() + 0.5);
        float g = (float) (rand.nextFloat() + 0.5);
        float b = (float) (rand.nextFloat() + 0.5);*/

        Color randomColor = new Color(r, g, b);        

        randomColor.brighter();

        return randomColor;
    }
}
