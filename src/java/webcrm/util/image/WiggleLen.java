/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.util.image;

/**
 *
 * @author eric0-kwong
 */

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;

public class WiggleLen {

    private int intWidth;
    private int intHeight;
    private int dt[];
    private int isle[];
    private int isle2[];
    private int cx;
    private int cy;
    private int icx;
    private int icy;
    private int ix;
    private int iy;
    private int bos;
    private int bgci = 0xffffff;
    private double td = 0.0;
    private Image bufferImage;

    public WiggleLen(Image img) {
        new WiggleLen(img, 0xFFFFFF);
    }

    public WiggleLen(Image inputImage, int intBackgroundColor) {

        bufferImage = inputImage;
        bgci = intBackgroundColor;

        intWidth = bufferImage.getWidth(null);
        intHeight = bufferImage.getHeight(null);

        bos = 0xff000000 | bgci;

        dt    = new int[intWidth * intHeight];
        isle  = new int[intWidth * intHeight];
        isle2 = new int[intWidth * intHeight];

        PixelGrabber pg = new PixelGrabber(bufferImage.getSource(), 0, 0, intWidth, intHeight, dt, 0, intWidth);
        try {
            pg.grabPixels();
        } catch (Exception _ex) {
        }

        icx = (int) Math.floor(intWidth  / 2);
        icy = (int) Math.floor(intHeight / 2);
        cx  = (int) Math.floor(intWidth  / 2);
        cy  = (int) Math.floor(intHeight / 2);

        for (int i = 0; i < intWidth; i++) {
            for (int j = 0; j < intHeight; j++) {
                if (i > cx - icx && i < cx + icx && j > cy - icy && j < cy + icy) {
                    isle2[j * intWidth + i] = dt[(i - cx) + icx + ((j - cy) + icy) * intWidth];
                } else {
                    isle2[j * intWidth + i] = 0xff000000 | bgci;
                }
            }
        }

        System.arraycopy(isle2, 0, isle, 0, intWidth * intHeight);
        MemoryImageSource MIS = new MemoryImageSource(intWidth, intHeight, isle, 0, intWidth);

        bufferImage = Toolkit.getDefaultToolkit().createImage(MIS);

    }

    public Image getImage() {
        return bufferImage;
    }

    //random move
    public void move(){
       int setting1 = 0;
       int setting2 = 0;

       do{
               setting1 = (int)(Math.random()*150);
       }while (setting1 < 50 || setting1 > 150);

       do{
               setting2 = (int)(Math.random()*1000);
       }while (setting2 < 400 || setting2 > 1000);

       move(setting1, setting2);
    }

    public void move(int setting1, int setting2) {

        double prm1 = (double) checkValue(setting1, 10, 120) / 10D;
        double prm2 = (double) checkValue(setting2, 10, 720) / 10D;

        for (int i = 0; i < intWidth; i++) {
            for (int j = 0; j < intHeight; j++) {
                isle[j * intWidth + i] = bos;
                iy = (int) ((double) j + prm1 * Math.cos((3.1415926535897931D / prm2) * (double) i + td));
                ix = (int) ((double) i + prm1 * Math.sin((3.1415926535897931D / prm2) * (double) j + td));

                if (ix >= 0 && iy >= 0 && ix < intWidth && iy < intHeight) {
                    isle[intWidth * j + i] = isle2[intWidth * iy + ix];
                }
            }
        }
        bufferImage.flush();
    }

    private int checkValue(int value, int radix, int defaultValue) {
        int intReturn = 0;
        try {
            intReturn = Integer.parseInt(value + "", radix);
        } catch (Exception _ex) {
            intReturn = defaultValue;
        }
        return intReturn;
    }
}
