/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.util.net.mail;

import java.util.Properties;
import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.MailcapCommandMap;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author eric0-kwong
 */
public class GMailServer {
    //format
    public static String PLAIN  = "text/plain";
    public static String HTML   = "text/html";

    public static enum ConnectionType{SSL, TLS};

    private ConnectionType  type            = null;
    private String          host            = null;
    private Integer         port            = null;
    private Address         from            = null;
    private String          user            = null;
    private String          password        = null;
    private String          protocol        = null;
    private String          sslFactory      = null;
    private Authenticator   authenticator   = null;

    public GMailServer(ConnectionType type, String host, Integer port, Address from, final String user, final String password){
        this.type       = type;
        this.host       = host;
        this.port       = port;
        this.from       = from;
        this.user       = user;
        this.password   = password;
        this.protocol   = "smtp";
        this.sslFactory = "javax.net.ssl.SSLSocketFactory";

        authenticator   = new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        };

        // add handlers for main MIME types
        MailcapCommandMap mc = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    public void sendPlainMessage(String to, String subject, String message) throws AddressException{
        send(new Address[]{new InternetAddress(to)}, null, null, subject, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address to, String subject, String message){
        send(new Address[]{to}, null, null, subject, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address[] to, String subject, String message){
        send(to, null, null, subject, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address[] to, String subject, String message, DataHandler[] attachments){
        send(to, null, null, subject, getBodyPart(PLAIN, null, message, null, attachments));
    }

    public void sendHTMLMessage(String to, String subject, String charset, String message) throws AddressException{
        send(new Address[]{new InternetAddress(to)}, null, null, subject, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address to, String subject, String charset, String message){
        send(new Address[]{to}, null, null, subject, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address[] to, String subject, String charset, String message){
        send(to, null, null, subject, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address[] to, String subject, String charset, String message, DataHandler[] inlines, DataHandler[] attachments){
        send(to, null, null, subject, getBodyPart(HTML, charset, message, inlines, attachments));
    }

    public Multipart getBodyPart(String format, String charset, String message, DataHandler[] inlines, DataHandler[] attachments) {
        //set content
        Multipart multipart = new MimeMultipart();//init MimeMultipart to store the BodyPart(s)
        BodyPart bodyPart = new MimeBodyPart();//init bodyPart
        try {
            //format "text/plain", "text/html" ...
            if(format.equals(PLAIN)){//set BodyPart content format and chartset
                //set BodyPart content format and chartset
                bodyPart.setContent(message, PLAIN);
            }else if(format.equals(HTML)){
                bodyPart.setContent(message, HTML + ";charset=" + charset);
            }

            multipart.addBodyPart(bodyPart);//add BodyPart to MimeMultipart
            if(inlines != null && inlines.length > 0){
                for(DataHandler inline: inlines){
                    bodyPart = new MimeBodyPart();//new  bodyPart for attachment
                    bodyPart.setDataHandler(inline);
                    bodyPart.setHeader("Content-ID", inline.getName());
                    bodyPart.setDisposition(BodyPart.INLINE);
                    bodyPart.setFileName(inline.getName());
                    multipart.addBodyPart(bodyPart);
                }
            }

            if(attachments != null && attachments.length > 0){
                for(DataHandler attachment: attachments){
                    bodyPart = new MimeBodyPart();//new  bodyPart for attachment
                    bodyPart.setDataHandler(attachment);
                    bodyPart.setDisposition(BodyPart.ATTACHMENT);
                    bodyPart.setFileName(attachment.getName());
                    multipart.addBodyPart(bodyPart);
                }
            }
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

        return multipart;
    }

    public void send(Address[] to, Address[] cc, Address[] bcc, String subject, Multipart multipart){
        Properties props = new Properties();
        props.put("mail.transport.protocol ", protocol);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");

        Session session = null;
        Transport transport = null;
        if(type == ConnectionType.SSL){
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", sslFactory);
            session = Session.getInstance(props, authenticator);
        }else if(type == ConnectionType.TLS){
            props.put("mail.smtp.starttls.enable", "true");
            session = Session.getInstance(props);
        }

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, to);

            if (cc != null){
                msg.setRecipients(Message.RecipientType.CC, cc);
            }

            if(bcc != null){
                msg.setRecipients(Message.RecipientType.BCC, bcc);
            }

            msg.setSubject(subject);
            msg.setContent(multipart);
            msg.saveChanges();


            // connect to the transport
            transport = session.getTransport("smtp");
            transport.connect(host, port, user, password);

            // send the msg and close the connection
            transport.sendMessage(msg, msg.getAllRecipients());


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            if(transport != null){
                try {
                    transport.close();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


}
