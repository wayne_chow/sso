/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.util.net.mail;

import com.sun.mail.smtp.SMTPTransport;
import java.util.Properties;
import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.MailcapCommandMap;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import net.oauth.OAuthConsumer;
import webcrm.util.net.mail.xoauth.XoauthAuthenticator;

/**
 *
 * @author eric0-kwong
 */
public class MailServer {
    //format
    public final static String PLAIN  = "text/plain";
    public final static String HTML   = "text/html";

    public static enum ConnectionType{SSL, TLS};

    private static enum AuthType{PASSWORD, OAUTH};

    private boolean         debug           = false;

    private AuthType        authType        = null;

    private ConnectionType  connectionType  = null;
    private String          host            = null;
    private Integer         port            = null;
    private Address         from            = null;
    private String          user            = null;
    private String          password        = null;
    private String          protocol        = "smtp";
    private String          sslFactory      = "javax.net.ssl.SSLSocketFactory";
    private Authenticator   authenticator   = null;

    private OAuthConsumer   oAuthConsumer   = null;


    static{
        // add handlers for main MIME types
        MailcapCommandMap mc = (MailcapCommandMap)CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }
    

    public MailServer(ConnectionType type, String host, Integer port, Address from, String oauthToken, String oauthTokenSecret, OAuthConsumer oAuthConsumer){

        this.authType    = AuthType.OAUTH;

        this.connectionType       = type;
        this.host       = host;
        this.port       = port;
        this.from       = from;
        this.user       = oauthToken;
        this.password   = oauthTokenSecret;
        

        if(oAuthConsumer == null){
            oAuthConsumer = XoauthAuthenticator.getAnonymousConsumer();
        }

        this.oAuthConsumer = oAuthConsumer;
    }

    public MailServer(ConnectionType type, String host, Integer port, Address from, final String user, final String password){

        this.authType    = AuthType.PASSWORD;
        
        this.connectionType       = type;
        this.host       = host;
        this.port       = port;
        this.from       = from;
        this.user       = user;
        this.password   = password;
        
        authenticator   = new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        };
    }

    public void sendPlainMessage(String to, String subject, String message) throws AddressException, MessagingException, Exception{
        send(new Address[]{new InternetAddress(to)}, null, null, subject, null, PLAIN, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address to, String subject, String message)throws MessagingException, Exception{
        send(new Address[]{to}, null, null, subject, null, PLAIN, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address[] to, String subject, String message)throws MessagingException, Exception{
        send(to, null, null, subject, null, PLAIN, getBodyPart(PLAIN, null, message, null, null));
    }

    public void sendPlainMessage(Address[] to, String subject, String message, DataHandler[] attachments)throws MessagingException, Exception{
        send(to, null, null, subject, null, PLAIN, getBodyPart(PLAIN, null, message, null, attachments));
    }

    public void sendHTMLMessage(String to, String subject, String charset, String message) throws AddressException, MessagingException, Exception{
        send(new Address[]{new InternetAddress(to)}, null, null, subject, charset, HTML, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address to, String subject, String charset, String message) throws MessagingException, Exception{
        send(new Address[]{to}, null, null, subject, charset, HTML, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address[] to, String subject, String charset, String message) throws MessagingException, Exception{
        send(to, null, null, subject, charset, HTML, getBodyPart(HTML, charset, message, null, null));
    }

    public void sendHTMLMessage(Address[] to, String subject, String charset, String message, DataHandler[] inlines, DataHandler[] attachments) throws MessagingException, Exception{
        send(to, null, null, subject, charset, HTML, getBodyPart(HTML, charset, message, inlines, attachments));
    }

    public Multipart getBodyPart(String format, String charset, String message, DataHandler[] inlines, DataHandler[] attachments) {
        //set content
        Multipart multipart = new MimeMultipart();//init MimeMultipart to store the BodyPart(s)
        BodyPart bodyPart = new MimeBodyPart();//init bodyPart
        try {
            //format "text/plain", "text/html" ...
            if(format.equals(PLAIN)){//set BodyPart content format and chartset
                //set BodyPart content format and chartset
                bodyPart.setContent(message, PLAIN);
            }else if(format.equals(HTML)){
                bodyPart.setContent(message, HTML + ";charset=" + charset);
            }

            multipart.addBodyPart(bodyPart);//add BodyPart to MimeMultipart
            if(inlines != null && inlines.length > 0){
                for(DataHandler inline: inlines){
                    bodyPart = new MimeBodyPart();//new  bodyPart for attachment
                    bodyPart.setDataHandler(inline);
                    bodyPart.setHeader("Content-ID", inline.getName());
                    bodyPart.setDisposition(BodyPart.INLINE);
                    bodyPart.setFileName(inline.getName());
                    multipart.addBodyPart(bodyPart);
                }
            }

            if(attachments != null && attachments.length > 0){
                for(DataHandler attachment: attachments){
                    bodyPart = new MimeBodyPart();//new  bodyPart for attachment
                    bodyPart.setDataHandler(attachment);
                    bodyPart.setDisposition(BodyPart.ATTACHMENT);
                    bodyPart.setFileName(attachment.getName());
                    multipart.addBodyPart(bodyPart);
                }
            }
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

        return multipart;
    }

    private Message getMessage(Session session, Address from, Address[] to, Address[] cc, Address[] bcc,
                                String subject, String charset, String format, Multipart multipart) throws MessagingException{

        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(from);
        msg.setRecipients(Message.RecipientType.TO, to);

        if (cc != null){
            msg.setRecipients(Message.RecipientType.CC, cc);
        }

        if(bcc != null){
            msg.setRecipients(Message.RecipientType.BCC, bcc);
        }

        if(format.equals(PLAIN)){//set BodyPart content format and chartset
            //set BodyPart content format and chartset
            msg.setHeader("Content-Type", PLAIN + ";charset=" + charset);
        }else if(format.equals(HTML)){
            msg.setHeader("Content-Type", HTML + ";charset=" + charset);
        }
        msg.setSubject(subject, charset);
        msg.setContent(multipart);
        msg.saveChanges();

        return msg;
    }

    public void send(Address[] to, Address[] cc, Address[] bcc, String subject, String charset, String format, Multipart multipart) throws MessagingException, Exception{
        if(authType == AuthType.PASSWORD){
            send_byPassword(to, cc, bcc, subject, charset, format, multipart);
        }else if(authType == AuthType.OAUTH){
            send_byOAuth(to, cc, bcc, subject, charset, format, multipart);
        }
        /*else{
            throw new NoSuchMethodException("AuthType Method not define.");
        }*/
    }

    private void send_byPassword(Address[] to, Address[] cc, Address[] bcc, String subject, String charset, String format, Multipart multipart) throws MessagingException{
        Properties props = new Properties();
        props.put("mail.transport.protocol ", protocol);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        
        Session session = null;
        Transport transport = null;
        
        try {
            if(connectionType == ConnectionType.SSL){
                props.put("mail.smtp.socketFactory.port", port);
                props.put("mail.smtp.socketFactory.class", sslFactory);
                session = Session.getInstance(props, authenticator);
                
                transport = session.getTransport("smtps");

            }else if(connectionType == ConnectionType.TLS){
                props.put("mail.smtp.starttls.enable", "true");
                session = Session.getInstance(props);
                transport = session.getTransport("smtp");
            }

            session.setDebug(debug);

            Message msg = getMessage(session, from, to, cc, bcc, subject, charset, format, multipart);

            // connect to the transport
            
            transport.connect(host, port, user, password); 

            // send the msg and close the connection
            transport.sendMessage(msg, msg.getAllRecipients());

        } finally{
            if(transport != null){
                try {
                    transport.close();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    private void send_byOAuth(Address[] to, Address[] cc, Address[] bcc, String subject, String charset, String format, Multipart multipart) throws MessagingException, Exception{
        Properties props = new Properties();

        props.put("mail.smtp.ehlo", "true");
        props.put("mail.smtp.auth", "false");
        //props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.smtp.starttls.required", "true");
        props.put("mail.smtp.sasl.enable", "false");

        boolean isSSL   = false;
        Session session = null;
        SMTPTransport transport = null;

        if(connectionType == ConnectionType.SSL){
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", sslFactory);
            session = Session.getInstance(props, authenticator);
            isSSL   = true;
        }else if(connectionType == ConnectionType.TLS){
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");
            session = Session.getInstance(props);
        }

        session.setDebug(debug);

        try {
            Message msg = getMessage(session, from, to, cc, bcc, subject, charset, format, multipart);

            transport = XoauthAuthenticator.connectToSmtp(session, host, port, ((javax.mail.internet.InternetAddress)from).getAddress(), user, password, oAuthConsumer, isSSL);
      
            transport.sendMessage(msg, msg.getAllRecipients());

        } finally{
            if(transport != null){
                try {
                    transport.close();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
