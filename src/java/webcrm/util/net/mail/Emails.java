/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrm.util.net.mail;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author eric0-kwong
 */
public class Emails {
    
    private Map<String, Email> emailTable = null;

    public Emails(){
        emailTable = new Hashtable<String, Email>();
    }

    public void add(String emailId, Email email){
        emailTable.put(emailId, email);
    }

    public Email get(String emailId){
        return emailTable.get(emailId);
    }
    
    public Email remove(String emailId){
        return emailTable.remove(emailId);
    }
    
    public void clear(){
        emailTable.clear();
    }

    public int size(){
        return emailTable.size();
    }

    public List<Email> getEmailList(){
        return new ArrayList(emailTable.values());
    }

    public class Email {

        private Address from = null;
        private List<Address> to = null;
        private List<Address> cc = null;
        private List<Address> bcc = null;
        private String subject = null;
        private String message = null;
        private Map<String, Attachment> attachmentTable;

        public Email() {

            this.attachmentTable = new Hashtable<String, Attachment>();
            /*
            MailServer mailServer = null;
            MimeMultipart mimeMultipart = null;

            if (attachments != null) {
            mimeMultipart = new MimeMultipart();
            for (Attachment attachment : attachments) {
            mimeMultipart.addBodyPart(attachment.getFile());
            }
            }

            mailServer.send(to.toArray(new Address[1]), cc.toArray(new Address[1]), bcc.toArray(new Address[1]), subject, mimeMultipart);
             */
        }

        public long getSize() {
            long size = 0;

            if (attachmentTable != null) {
                for (Attachment attachment : attachmentTable.values()) {
                    size += attachment.getSize();
                }
            }

            return size;
        }

        public List<Attachment> getAttachments() {
            return new ArrayList(attachmentTable.values());
        }

        public void addAttachment(String attachmentId, MimeBodyPart attachment, long sizeInKB) {
            attachmentTable.put(attachmentId, new Attachment(attachment, sizeInKB));
        }

        public Attachment removeAttachment(String attachmentId) {
            return attachmentTable.remove(attachmentId);
        }

        public List<Address> getBcc() {
            return bcc;
        }

        public void setBcc(List<Address> bcc) {
            this.bcc = bcc;
        }

        public List<Address> getCc() {
            return cc;
        }

        public void setCc(List<Address> cc) {
            this.cc = cc;
        }

        public Address getFrom() {
            return from;
        }

        public void setFrom(Address from) {
            this.from = from;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public List<Address> getTo() {
            return to;
        }

        public void setTo(List<Address> to) {
            this.to = to;
        }
    }

    public class Attachment {

        long size;
        MimeBodyPart file;

        public Attachment(MimeBodyPart file, long size) {
            this.file = file;
            this.size = size;
        }

        public MimeBodyPart getFile() {
            return file;
        }

        public long getSize() {
            return size;
        }
    }
}
