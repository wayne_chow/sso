/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.bean;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author ken0-lee
 */
public class InstrumentedHashMap<K, V>{
    private Map<K, V> map;
    private Queue<K> queue;
    private int capacity = 50;

    private static InstrumentedHashMap<String, String> authCodeHashMap = new InstrumentedHashMap<String, String>(50);

    public static InstrumentedHashMap<String, String> getAuthCodeHashMap(){
        return authCodeHashMap;
    }

    private InstrumentedHashMap(int capacity) {
        map = new HashMap<K, V>();
        queue = new LinkedBlockingQueue<K>(capacity);
        this.capacity = capacity;
    }

    public V put(K key, V value) {
        if (map.size() >= capacity && !map.containsKey(key)) {
            map.remove(queue.remove());
        }
        queue.offer(key);
        return map.put(key, value);
    }

    public V remove(K key){
        queue.remove(key);
        return map.remove(key);
    }
}