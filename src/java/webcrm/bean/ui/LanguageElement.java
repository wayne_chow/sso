/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcrm.bean.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author eric0-kwong
 */
public class LanguageElement {

    //Locale.getAvailableLocales() != locales[]
    private static Locale locales[] = {
        Locale.CANADA,
        Locale.CANADA_FRENCH,
        Locale.CHINA,
        Locale.CHINESE,
        Locale.ENGLISH,
        Locale.FRANCE,
        Locale.FRENCH,
        Locale.GERMAN,
        Locale.GERMANY,
        Locale.ITALIAN,
        Locale.ITALY,
        Locale.JAPAN,
        Locale.JAPANESE,
        Locale.KOREA,
        Locale.KOREAN,
        Locale.PRC,
        Locale.ROOT,
        Locale.SIMPLIFIED_CHINESE,
        Locale.TAIWAN,
        Locale.TRADITIONAL_CHINESE,
        Locale.UK,
        Locale.US
    };

    private static List<Locale> verticalLocales = Arrays.asList(
        Locale.CHINA,
        Locale.CHINESE,
        Locale.SIMPLIFIED_CHINESE,
        Locale.TAIWAN,
        Locale.TRADITIONAL_CHINESE,
        Locale.JAPAN,
        Locale.JAPANESE,
        Locale.KOREA,
        Locale.KOREAN
    );

    private static Map<String, Locale> localeTable = getLocaleTable();

    private String title;
    private String language;
    private String country;
    private String variant;

    private List<LanguageElement> languageElementList;
    
   public LanguageElement(){
        super();
        language    = null;
        country     = null;
        variant     = null;
        title       = null;
        languageElementList = null;
    }

    public List<LanguageElement> getLanguageElementList() {
        return languageElementList;
    }

    public void setLanguageElementList(List<LanguageElement> languageElementList) {
        this.languageElementList = languageElementList;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocaleCode(){
        return getLocaleCode(getLocale(language, country, variant));
    }
    
    public Locale getLocale(){
        return getLocale(language, country, variant);
    }

    public boolean hasAuthority(Locale locale){
        boolean hasAuthority = false;

        if(languageElementList != null){
            for(LanguageElement languageElement : languageElementList){
                if(languageElement.getLocale() == locale){
                    hasAuthority = true;
                    break;
                }
            }
        }
        
        return hasAuthority;
    }
    
    private static Map<String, Locale> getLocaleTable(){
        Hashtable<String, Locale> locTable = new Hashtable<String, Locale>();
        
        String key = null;
        for(Locale locale : locales){
            key = LanguageElement.getLocaleCode(locale);
            locTable.put(key, locale);
        }
        
        return locTable;
    }

    public static Locale getLocale(String language, String country, String variant){

        StringBuilder key = new StringBuilder();
        key.append(language);
        key.append("_");

        if(country != null)
            key.append(country);

        key.append("_");

        if(variant != null)
            key.append(variant);

        return localeTable.get(key.toString());
    }

    public static Locale getLocale(String localeCode){
        if(localeCode != null){
            return localeTable.get(localeCode);
        }else{
            return null;
        }
    }

    public static String getLocaleCode(Locale locale){
        StringBuilder key = new StringBuilder();

        key.append(locale.getLanguage());
        key.append("_");

        if(locale.getCountry() != null)
            key.append(locale.getCountry());

        key.append("_");

        if(locale.getVariant() != null)
            key.append(locale.getVariant());

        return key.toString();
    }

    public static boolean isVerticalDisplay(Locale locale){
        return verticalLocales.contains(locale);
    }

}
