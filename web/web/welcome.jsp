
<%@ page import="java.util.ResourceBundle,java.util.Locale,java.util.HashMap,webcrm.bean.ui.LanguageElement" %>
<%@ page import="webcrm.util.SSOProperties" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%

            response.setContentType("text/html;charset=UTF-8");


            String userId = request.getParameter("userid");
            String password = request.getParameter("password");
            String language = request.getParameter("language");

            if(language != null){
                Locale changeLocale = LanguageElement.getLocale(language);
                if(changeLocale != null)
                    session.setAttribute("language", changeLocale);
            }

            Locale locale = (Locale) session.getAttribute("language");

            ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

            HashMap<String, String> hm = (HashMap) session.getAttribute("errorAccess");

            //remove before error in this session
            session.removeAttribute("errorAccess");

            //String invalidLoginOrPassword = "";
            String invalidSessionTimeout = "";

            if (hm != null) {
//        if(hm.containsKey("error.message")){
//            invalidLoginOrPassword = hm.get("error.message");
//	}
                if (hm.containsKey("error.sessiontimeout")) {
                    //invalidSessionTimeout = "<font color=\"#ff0000\">" + bundle.getString("loginPage.error.sessiontimeout") + "</font>";
                    invalidSessionTimeout = bundle.getString("loginPage.error.sessiontimeout");
                }
            }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/SSO/css/bootstrap.css" rel="stylesheet">
        <link href="/SSO/css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="/SSO/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/SSO/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/SSO/js/cookie/jquery.cookie.js"></script>
        <title><%=bundle.getString("loginPage.caption.singleSignOn")%></title>

        <style type="text/css">
            body {
                margin: 0px auto;
                padding: 0px;                
                background-position:  center center;
                width: 1280px;
                vertical-align: top;
                background-color: #cccccc;
                background-repeat: no-repeat;
                height: 100%;
                text-align: left;
            }

            div.button{
                height: 172px;
                width: 172px;
            }
            
            div.clickable{
                cursor : pointer;
            }

            div.blocked{
                cursor : not-allowed;
            }
        </style>

        <script>

            $(document).ready(function(){

<%
                String isWSMValid = request.getParameter("isWSMValid");
                String isPSMValid = request.getParameter("isPSMValid");
                if(isWSMValid != null && isWSMValid.equals("Y") &&
                        isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $("div.button").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#wsm").click(loginWSM);
                    $("#psm").click(loginPSM);
<%
                }else if(isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $("div.button:not(#wsm)").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#psm").click(loginPSM);
                    //$("#message").html("<%=bundle.getString("loginPage.caption.wsmLoginFail")%>");
<%
                }else if(isWSMValid != null && isWSMValid.equals("Y")){
%>
                    $("div.button:not(#psm)").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#wsm").click(loginWSM);
                    //$("#message").html("<%=bundle.getString("loginPage.caption.psmLoginFail")%>");
<%
                }
%>

                var cookieOption = { path: '/', expires: 1 };

<%
                if(invalidSessionTimeout == null || invalidSessionTimeout.isEmpty()){
                }else{
%>
                    $.cookie("COOKIE_USERID", null, cookieOption);
                    $.cookie("COOKIE_PASSWORD", null, cookieOption);
                    //$.cookie("COOKIE_LANGUAGE", null, cookieOption);
<%
                }
%>
                var cookieUserId   = $.cookie("COOKIE_USERID");
                var cookiePassword = $.cookie("COOKIE_PASSWORD");

                if(cookieUserId != null)
                    $(".param input[name=userid]").val(cookieUserId);
                if(cookiePassword != null)
                    $(".param input[name=password]").val(cookiePassword);

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);

                $("label[name=language]").click(function(){
                    //var language = $("option:selected", this).val();
                    var language = $(this).attr("value");
                    var userId   = $(".param input[name=userid]").val();
                    var password = $(".param input[name=password]").val();

                    var date = new Date();
                    date.setTime(date.getTime() + (60 * 1000));
                    $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                    $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });

                    var languageDate = new Date();
                    languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                    $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                    changeLanguage(language);
                });

                $("#btn_release").click(release);
            });


            function release(){
                window.location.href = "/SSO";
            }

            // login to wsm and redirect the user to the new page
            function loginWSM(){
                var isSessionValid = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/SSO/VerifySession",
                    success: function(data){
                        if(data == true){
                            isSessionValid = true;
                        }
                    },
                    dataType: "json"
                });

                if(isSessionValid){
                    var form = $("#WSMLoginForm");
                    if(form.length == 0){//create form
                        var form = $("<form method='post' id='WSMLoginForm' target='wsm' action='<%=SSOProperties.getProperty("WSM.LOGIN.LINK")%>'></form>");
                        form.append("<input type='hidden' name='origin' value='SSO' />");
                        form.append("<input type='hidden' name='userid' value='' />");
                        form.append("<input type='hidden' name='password' value='' />");
                        form.append("<input type='hidden' name='language' value='' />");
                        form.append("<input type='hidden' name='company' value='AMG' />");
                        $("body").append(form);
                    }
                    form.children("input[name=userid]").val($(".param input[name=userid]").val());
                    form.children("input[name=password]").val($(".param input[name=password]").val());
                    form.children("input[name=language]").val($(".param input[name=language]").val());
                    form.submit();
                }else{
                    // similar behavior as an HTTP redirect
                    //window.location.replace("/SSO");

                    // similar behavior as clicking on a link
                    window.location.href = "/SSO";
                }
            }

            // login to psm and redirect the user to the new page
            function loginPSM(){
                var isSessionValid = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/SSO/VerifySession",
                    success: function(data){
                        if(data == true){
                            isSessionValid = true;
                        }
                    },
                    dataType: "json"
                });

                if(isSessionValid){
                    var form = $("#PSMLoginForm");
                    if(form.length == 0){//create form
                        var form = $("<form method='post' id='PSMLoginForm' target='psm' action='<%=SSOProperties.getProperty("PSM.LOGIN.LINK")%>'></form>");
                        form.append("<input type='hidden' name='Origin' value='sso' />");
                        form.append("<input type='hidden' name='UserId' value='' />");
                        form.append("<input type='hidden' name='Password' value='' />");
                        form.append("<input type='hidden' name='language' value='' />");
                        $("body").append(form);
                    }
                    form.children("input[name=UserId]").val($(".param input[name=userid]").val());
                    form.children("input[name=Password]").val($(".param input[name=password]").val());
                    //convert to psm variable
                    var lang = $(".param input[name=language]").val();
                    if(lang == "en__"){
                        lang = "1";
                    }else if(lang == "zh_TW_"){
                        lang = "2";
                    }else if(lang == "zh_CN_"){
                        lang = "3";
                    }else{
                        lang = "1";
                    }
                    form.children("input[name=language]").val(lang);
                    form.submit();
                }else{
                    // similar behavior as an HTTP redirect
                    //window.location.replace("/SSO");

                    // similar behavior as clicking on a link
                    window.location.href = "/SSO";
                }
            }

            //Change language function
            function changeLanguage(language){
                var form = $("#buttonMenuForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='buttonMenuForm' target='_self' action='/SSO/web/welcome.jsp'></form>");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='isPSMValid' value='' />");
                    form.append("<input type='hidden' name='isWSMValid' value='' />");
                    $("body").append(form);
                }
                form.children("input[name=userid]").val("<%=userId%>");
                form.children("input[name=password]").val("<%=password%>");
                form.children("input[name=language]").val(language);
                form.children("input[name=isPSMValid]").val("<%=isPSMValid%>");
                form.children("input[name=isWSMValid]").val("<%=isWSMValid%>");
                form.submit();
/*
                $.post("/SSO/changeLanguage.action", { "language": language},
                    function(data){
                        location.reload(true);
                });
                */
            }

        </script>
    </head>
    <body style="height: 781px;">
        <table style="text-align: left; width: 1280px; height: 775px;background-image: url('/SSO/img/P2.jpeg');" border="0" cellpadding="0" cellspacing="2">
            <tbody>
                <tr>
                    <td style="vertical-align: top; width: 300px; height: 46px;"> <br>
                        <div class="param">
                        <input type="hidden" name="userid" value="<%=(userId==null)?"":userId%>"/>
                        <input type="hidden" name="password" value="<%=(password==null)?"":password%>"/>
                        <input type="hidden" name="language" value="<%=(language==null)?"":language%>"/>
                        </div>
                    </td>
                    <td style="vertical-align: top; width: 172px;" > <br> </td>
                    <td style="vertical-align: top; width: 78px;"> <br> </td>
                    <td style="vertical-align: top; width: 172px;"> <br> </td>
                    <td style="vertical-align: top; width: 78px;"> <br> </td>
                    <td style="vertical-align: top; width: 172px;"> <br> </td>
                    <td style="vertical-align: top; height: 46px; text-align:center">
                        <div style="padding-top:10px; color: #acce22; font-size:24px">
                            <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.simplifiedChinese")%></label> |
                            <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.traditionalChinese")%></label> |
                            <label name="language" value="en__" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.english")%></label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"> <br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked" id="psm" style="background:url(/SSO/img/tra1.png)" outImage="url(/SSO/img/tra1.png)" overImage="url(/SSO/img/tra2.png)"/>
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked"  id="prod" style="background:url(/SSO/img/prod1.png)" outImage="url(/SSO/img/prod1.png)" overImage="url(/SSO/img/prod2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked"  id="vip" style="background:url(/SSO/img/vip1.png)" outImage="url(/SSO/img/vip1.png)" overImage="url(/SSO/img/vip2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td style="height: 44px;" colspan="7"><br></td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked" id="wsm" style="background:url(/SSO/img/acc1.png)" outImage="url(/SSO/img/acc1.png)" overImage="url(/SSO/img/acc2.png)"/>
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="trust" style="background:url(/SSO/img/trust1.png)" outImage="url(/SSO/img/trust1.png)" overImage="url(/SSO/img/trust2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="market" style="background:url(/SSO/img/market1.png)" outImage="url(/SSO/img/market1.png)" overImage="url(/SSO/img/market2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td style="height: 44px;" colspan="7"><br></td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="service" style="background:url(/SSO/img/ser1.png)" outImage="url(/SSO/img/ser1.png)" overImage="url(/SSO/img/ser2.png)"  />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="land" style="background:url(/SSO/img/land1.png)" outImage="url(/SSO/img/land1.png)" overImage="url(/SSO/img/land2.png)"  />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="other" style="background:url(/SSO/img/other1.png)" outImage="url(/SSO/img/other1.png)" overImage="url(/SSO/img/other2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align:center">
                        <button type="button" id="btn_release" class="btn btn-lg btn-primary"><%=bundle.getString("loginPage.button.exit")%></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>