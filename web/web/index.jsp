
<%@ page import="java.util.ResourceBundle,java.util.Locale,java.util.HashMap,webcrm.bean.ui.LanguageElement" %>
<%@ page import="webcrm.util.SSOProperties" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%

            response.setContentType("text/html;charset=UTF-8");

            Locale locale = (Locale) session.getAttribute("language");

            ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

            String localeCode = LanguageElement.getLocaleCode(locale);

            HashMap<String, String> hm = (HashMap) session.getAttribute("errorAccess");

            //remove before error in this session
            session.removeAttribute("errorAccess");

            //String invalidLoginOrPassword = "";
            String invalidSessionTimeout = "";

            if (hm != null) {
//        if(hm.containsKey("error.message")){
//            invalidLoginOrPassword = hm.get("error.message");
//	}
                if (hm.containsKey("error.sessiontimeout")) {
                    //invalidSessionTimeout = "<font color=\"#ff0000\">" + bundle.getString("loginPage.error.sessiontimeout") + "</font>";
                    invalidSessionTimeout = bundle.getString("loginPage.error.sessiontimeout");
                }
            }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/SSO/css/bootstrap.css" rel="stylesheet">
        <link href="/SSO/css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="/SSO/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/SSO/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/SSO/js/cookie/jquery.cookie.js"></script>
        <title><%=bundle.getString("loginPage.caption.singleSignOn")%></title>
        <style type="text/css">
            <%--body {
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
                padding-top: 0px;
                padding-right: 0px;
                padding-bottom: 0px;
                padding-left: 0px;
                height: 100%;
                vertical-align: top;
                background-color: #cccccc;
                text-align: left;
                background-repeat: no-repeat;
                background-position: center top;
                color: red;
            }--%>

            #mainContent
                {
                    width:1280px;
                    height:800px;
                    background-image: url("/SSO/img/P1.jpeg");
                    background-repeat: no-repeat;
                    background-position: center ;
                }

        </style>

        <script>

            $(document).ready(function(){
                var cookieOption = { path: '/', expires: 1 };

                $("#buttonPanel").hide();

<%
                if(invalidSessionTimeout == null || invalidSessionTimeout.isEmpty()){
%>
                    $("#message").hide();
<%
                }else{
%>
                    $("#message").text("<%=invalidSessionTimeout%>");
                    $.cookie("COOKIE_USERID", null, cookieOption);
                    $.cookie("COOKIE_PASSWORD", null, cookieOption);
                    //$.cookie("COOKIE_LANGUAGE", null, cookieOption);
<%
                }
%>
                var cookieUserId   = $.cookie("COOKIE_USERID");
                var cookiePassword = $.cookie("COOKIE_PASSWORD");
                //var cookieLanguage = $.cookie("COOKIE_LANGUAGE");

                if(cookieUserId != null)
                    $("#SSOForm input[name=userid]").val(cookieUserId);
                if(cookiePassword != null)
                    $("#SSOForm input[name=password]").val(cookiePassword);
                //if(cookieLanguage != null)
                //    $("#SSOForm select[name=language]").val(cookieLanguage);

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);


                //select language option
                <%--$("#SSOForm select[name=language] option[value='<%=localeCode%>']").attr('selected', 'selected');--%>

                //$("#SSOForm select[name=language]").change(function(){
                $("#SSOForm label[name=language]").click(function(){
                    //var language = $("option:selected", this).val();
                    var language = $(this).attr("value");
                    var userId   = $("#SSOForm input[name=userid]").val();
                    var password = $("#SSOForm input[name=password]").val();

                    var date = new Date();
                    date.setTime(date.getTime() + (60 * 1000));
                    $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                    $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });

                    var languageDate = new Date();
                    languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                    $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                    changeLanguage(language);
                });

                $("#SSOForm input").keypress(function(e) {
                        if(e.which == 13 && $("#btn_checkLogin").is(":visible")) {
                            checkLogin();
                        }
                    });

                $("#btn_checkLogin").click(checkLogin);

                $("#SSOForm input[name=userid]").focus();
            });

            function checkLogin(){
                var userid = $("#SSOForm input[name=userid]").val();
                var password = $("#SSOForm input[name=password]").val();
                if(userid == null || userid == ""){//validate the input user id
                    $("#message").text("<%=bundle.getString("loginPage.caption.UserIdShouldNotBeEmpty")%>");
                    $("#message").show();
                }else if(password == null || password == ""){//validate the input password
                    $("#message").text("<%=bundle.getString("loginPage.caption.PasswordShouldNotBeEmpty")%>");
                    $("#message").show();
                }else{
                    $("#message").hide();
                    $("#message").text("");
                    /*
                    $("#SSOForm input[name=userid]").attr("readonly", true);
                    $("#SSOForm input[name=password]").attr("readonly", true);
                    $("#SSOForm select[name=language]").attr("readonly", true).attr("disabled", true);
                    */
                    //$("#btn_checkLogin").hide();


                    //renew the session
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "/SSO/SignIn"
                    });

                    var psmUserValid = false;
                    var wsmUserValid = false;

                    //check psm login
                    $.ajax({
                        async: false,
                        type: "GET",
                        url: "<%=SSOProperties.getProperty("PSM.USERVALIDATION.LINK")%>",
                        data: {
                            "username": userid,
                            "password": password
                        },
                        success: function(data){
                            if(data == true){
                                psmUserValid = true;
                            }
                        },
                        dataType: "json"
                    });

                    // check wsm login
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<%=SSOProperties.getProperty("WSM.USERVALIDATION.LINK")%>",
                        data: {
                            "userid"    : userid,
                            "password"  : password,
                            "company"   : "AMG"
                        },
                        success: function(data){
                            if(data == true){
                                wsmUserValid = true;
                            }
                        },
                        dataType: "json"
                    });

                    if(psmUserValid && wsmUserValid){//both system user valid, redirect to button menu page
                        redirectToButtonMenu("Y", "Y");
                    }else if(psmUserValid){//psm user valid, redirect to button menu page
                        redirectToButtonMenu("Y", "N");
                    }else if(wsmUserValid){//wsm user valid, redirect to button menu page
                        redirectToButtonMenu("N", "Y");
                    }else{//both system user invalid, show login fail msg for both system.
                        var msg = "<%=bundle.getString("loginPage.caption.wsmLoginFail")%><br/><%=bundle.getString("loginPage.caption.psmLoginFail")%>";
                        $("#message").html(msg);
                        $("#message").show();
                    }

                }
            }


            //Change language function
            function changeLanguage(language){
                $.post("/SSO/changeLanguage.action", { "language": language },
                    function(data){
                        location.reload(true);
                });
            }

            // go to buttonMenu.jsp
            function redirectToButtonMenu(isPSMValid, isWSMValid){
                var form = $("#buttonMenuForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='buttonMenuForm' target='_self' action='/SSO/web/welcome.jsp'></form>");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='isPSMValid' value='' />");
                    form.append("<input type='hidden' name='isWSMValid' value='' />");
                    $("body").append(form);
                }
                form.children("input[name=userid]").val($("#SSOForm input[name=userid]").val());
                form.children("input[name=password]").val($("#SSOForm input[name=password]").val());
                form.children("input[name=language]").val("<%=localeCode%>");
                form.children("input[name=isPSMValid]").val(isPSMValid);
                form.children("input[name=isWSMValid]").val(isWSMValid);
                form.submit();
            }
        </script>
    </head>
    <body lang="zh-cn">
    <center>
        <div id="mainContent">
        <form id="SSOForm">
            <%--<table style="width: 1280px; height:800px; margin-right: 8px; background-image: url('/SSO/img/P1.jpeg'); background-repeat: no-repeat;">--%>
            <table style="margin-right: 8px;">
                <tbody>
                    <tr>
                        <td rowspan="2" style="width: 960px;">
                            &nbsp;
                        </td>
                        <td style="vertical-align: top; width: 320px; height: 280px; text-align: center;">
                            <div style="padding-top:10px; color: #acce22; font-size:24px">
                                <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.simplifiedChinese")%></label> |
                                <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.traditionalChinese")%></label> |
                                <label name="language" value="en__" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.english")%></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 12px; vertical-align: top; height: 587px;">
                            <table border="0" cellpadding="2" cellspacing="2">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle; width: 100px; text-align: center; font-family: Arial;">
                                            <big><span style="color: rgb(0, 0, 153);"><%=bundle.getString("loginPage.caption.userId")%> </span> </big>
                                        </td>
                                        <td style="vertical-align: middle; width: 202px; text-align: center;">
                                            <input type="text" name="userid" tabindex="1" autofocus="" placeholder="<%=bundle.getString("loginPage.caption.userId")%>" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; font-family: Arial; color: rgb(0, 0, 153); text-align: center;">
                                            <big><%=bundle.getString("loginPage.caption.password")%></big>
                                        </td>
                                        <td style="vertical-align: middle; text-align: center;">
                                            <input type="password" name="password" tabindex="2" placeholder="<%=bundle.getString("loginPage.caption.password")%>">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="width:100%;"border="0" cellpadding="2" cellspacing="2">
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="padding-top: 12px; vertical-align: middle; text-align: center;">
                                            <button type="button" id="btn_checkLogin" style="height:30px; padding-top:0px; padding-bottom:0px" class="btn btn-lg btn-primary"><%=bundle.getString("loginPage.button.signIn")%></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center; padding: 5px 5px 0 0;">
                                            <a id="forgetPasswordLink"href="/SSO/web/forgetPassword.jsp"><%=bundle.getString("loginPage.caption.forgetPassword")%></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div id="message" style="color:#ff0000; padding-left: 8px; text-align:center"></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
        </div>
    </center>
    </body>
</html>
