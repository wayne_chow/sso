<%-- 
    Document   : login
    Created on : 30-Oct-2013, 12:10:59
    Author     : ken0-lee
--%>

<%@ page import="java.util.ResourceBundle,java.util.Locale,java.util.HashMap,webcrm.bean.ui.LanguageElement" %>
<%@ page import="webcrm.util.SSOProperties" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%

    response.setContentType("text/html;charset=UTF-8");

    Locale locale = (Locale)session.getAttribute("language");
    
    ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

    String localeCode = LanguageElement.getLocaleCode(locale);

    HashMap<String, String> hm = (HashMap)session.getAttribute("errorAccess");

    //remove before error in this session
    session.removeAttribute("errorAccess");

    //String invalidLoginOrPassword = "";
    String invalidSessionTimeout  = "";

    if(hm != null){
//        if(hm.containsKey("error.message")){
//            invalidLoginOrPassword = hm.get("error.message");
//	}
        if(hm.containsKey("error.sessiontimeout")){
            //invalidSessionTimeout = "<font color=\"#ff0000\">" + bundle.getString("loginPage.error.sessiontimeout") + "</font>";
            invalidSessionTimeout = bundle.getString("loginPage.error.sessiontimeout");
        }
    }

%>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/SSO/css/bootstrap.css" rel="stylesheet">
        <link href="/SSO/css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="/SSO/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/SSO/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/SSO/js/cookie/jquery.cookie.js"></script>
        <title><%=bundle.getString("loginPage.caption.singleSignOn")%></title>
        <style>
            body {
                padding-top: 10px;
                padding-bottom: 10px;
                /*background-color: #eee;*/
                background-image:url('/SSO/img/background.png');
                background-position-x: 50%;
                /*background-position-y: 40px;*/
                background-size: initial;
                background-repeat-x: repeat;
                background-repeat-y: no-repeat;
                background-attachment: initial;
                background-origin: initial;
                background-clip: initial;
                background-color: rgb(245, 245, 245);
            }

            .form-signin {
              max-width: 580px;
              padding: 10px;
              margin: 0 auto;
            }
            .form-signin .form-signin-heading {
              margin-bottom: 10px;
              text-align:center;
            }
            .form-signin .form-control {
              position: relative;
              font-size: 16px;
              height: auto;
              padding: 10px;
              -webkit-box-sizing: border-box;
                 -moz-box-sizing: border-box;
                      box-sizing: border-box;
            }
            .form-signin .form-control:focus {
              z-index: 2;
            }
            .form-signin input[type="text"], .form-signin input[type="password"] {
              margin-bottom: -1px;
              border-bottom-left-radius: 0;
              border-bottom-right-radius: 0;
            }
            .form-signin select[name="language"] {
              /*margin-bottom: 10px;*/
              border-top-left-radius: 0;
              border-top-right-radius: 0;
            }
            .form-center td{
                padding-left: 5px;
            }

            .redirect-button{
                max-width: 1000px;
                margin: 0 auto;
            }

            .redirect-button button{
                width: 150px;
                height: 60px;
                margin: 10px;
            }
        </style>
        <script>

            $(document).ready(function(){

<%
                String isWSMValid = request.getParameter("isWSMValid");
                String isPSMValid = request.getParameter("isPSMValid");
                if(isWSMValid != null && isWSMValid.equals("Y") &&
                        isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $(".redirect-button button").removeAttr("disabled").removeClass("btn-default").addClass("btn-info");
<%
                }else if(isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $(".redirect-button button:not(#wsm)").removeAttr("disabled").removeClass("btn-default").addClass("btn-info");
                    //$("#message").html("<%=bundle.getString("loginPage.caption.wsmLoginFail")%>");

<%
                }else if(isWSMValid != null && isWSMValid.equals("Y")){
%>
                    $(".redirect-button button:not(#psm)").removeAttr("disabled").removeClass("btn-default").addClass("btn-info");
                    //$("#message").html("<%=bundle.getString("loginPage.caption.psmLoginFail")%>");
<%
                }
%>

                $("#btn_release").click(release);

                $("#wsm").click(loginWSM);
                $("#psm").click(loginPSM);
            });


            function release(){
                window.location.href = "/SSO";
            }

            // login to wsm and redirect the user to the new page
            function loginWSM(){
                var isSessionValid = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/SSO/VerifySession",
                    success: function(data){
                        if(data == true){
                            isSessionValid = true;
                        }
                    },
                    dataType: "json"
                });

                if(isSessionValid){
                    var form = $("#WSMLoginForm");
                    if(form.length == 0){//create form
                        var form = $("<form method='post' id='WSMLoginForm' target='wsm' action='<%=SSOProperties.getProperty("WSM.LOGIN.LINK")%>'></form>");
                        form.append("<input type='hidden' name='origin' value='SSO' />");
                        form.append("<input type='hidden' name='userid' value='' />");
                        form.append("<input type='hidden' name='password' value='' />");
                        form.append("<input type='hidden' name='language' value='' />");
                        form.append("<input type='hidden' name='company' value='AMG' />");
                        $("body").append(form);
                    }
                    form.children("input[name=userid]").val($(".jumbotron input[name=userid]").val());
                    form.children("input[name=password]").val($(".jumbotron input[name=password]").val());
                    form.children("input[name=language]").val($(".jumbotron input[name=language]").val());
                    form.submit();
                }else{
                    // similar behavior as an HTTP redirect
                    //window.location.replace("/SSO");

                    // similar behavior as clicking on a link
                    window.location.href = "/SSO";
                }
            }

            // login to psm and redirect the user to the new page
            function loginPSM(){
                var isSessionValid = false;
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "/SSO/VerifySession",
                    success: function(data){
                        if(data == true){
                            isSessionValid = true;
                        }
                    },
                    dataType: "json"
                });

                if(isSessionValid){
                    var form = $("#PSMLoginForm");
                    if(form.length == 0){//create form
                        var form = $("<form method='post' id='PSMLoginForm' target='psm' action='<%=SSOProperties.getProperty("PSM.LOGIN.LINK")%>'></form>");
                        form.append("<input type='hidden' name='Origin' value='sso' />");
                        form.append("<input type='hidden' name='Email' value='' />");
                        form.append("<input type='hidden' name='Password' value='' />");
                        form.append("<input type='hidden' name='language' value='' />");
                        $("body").append(form);
                    }
                    form.children("input[name=userid]").val($(".jumbotron input[name=userid]").val());
                    form.children("input[name=password]").val($(".jumbotron input[name=password]").val());
                    form.children("input[name=language]").val($(".jumbotron input[name=language]").val());
                    form.submit();
                }else{
                    // similar behavior as an HTTP redirect
                    //window.location.replace("/SSO");

                    // similar behavior as clicking on a link
                    window.location.href = "/SSO";
                }
            }
        </script>
    </head>
    <body>
        <div class="container">

            <div class="jumbotron" style="opacity: 0.95;padding-top:5px;padding-bottom:15px" align="center">
                <h2 class="form-signin-heading"><%=bundle.getString("loginPage.caption.welcome")%></h2>
                <%--<div id="message" class="alert alert-warning" align="left" style="padding-top:5px;padding-bottom:5px"></div>--%>

                <input type="hidden" name="userid" value="<%=request.getParameter("userid")%>"/>
                <input type="hidden" name="password" value="<%=request.getParameter("password")%>"/>
                <input type="hidden" name="language" value="<%=request.getParameter("language")%>"/>

                <div class="redirect-button" id="buttonPanel" align="left">
                    <button id="wsm" class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.accountInfo")%></button>
                    <button id="psm" class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.psm")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.vipInfo")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.productInfo")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.serviceEnquiry")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.travelInfo")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.marketInfo")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.trustInfo")%></button>
                    <button class="btn btn-lg btn-default" disabled><%=bundle.getString("loginPage.button.other")%></button>
                </div>
                <button type="button" id="btn_release" class="btn btn-lg btn-primary"><%=bundle.getString("loginPage.button.exit")%></button>
            </div>
        </div>
    </body>
</html>
