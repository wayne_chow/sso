<%-- 
    Document   : login
    Created on : 30-Oct-2013, 12:10:59
    Author     : ken0-lee
--%>

<%@ page import="java.util.ResourceBundle,java.util.Locale,java.util.HashMap,webcrm.bean.ui.LanguageElement" %>
<%@ page import="webcrm.util.SSOProperties" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%

    response.setContentType("text/html;charset=UTF-8");

    Locale locale = (Locale)session.getAttribute("language");
    
    ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

    String localeCode = LanguageElement.getLocaleCode(locale);

    HashMap<String, String> hm = (HashMap)session.getAttribute("errorAccess");

    //remove before error in this session
    session.removeAttribute("errorAccess");

    //String invalidLoginOrPassword = "";
    String invalidSessionTimeout  = "";

    if(hm != null){
//        if(hm.containsKey("error.message")){
//            invalidLoginOrPassword = hm.get("error.message");
//	}
        if(hm.containsKey("error.sessiontimeout")){
            //invalidSessionTimeout = "<font color=\"#ff0000\">" + bundle.getString("loginPage.error.sessiontimeout") + "</font>";
            invalidSessionTimeout = bundle.getString("loginPage.error.sessiontimeout");
        }
    }

%>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/SSO/css/bootstrap.css" rel="stylesheet">
        <link href="/SSO/css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="/SSO/js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="/SSO/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/SSO/js/cookie/jquery.cookie.js"></script>
        <title><%=bundle.getString("loginPage.caption.singleSignOn")%></title>
        <style>
            body {
                padding-top: 10px;
                padding-bottom: 10px;
                /*background-color: #eee;*/
                background-image:url('/SSO/img/background.png');
                background-position-x: 50%;
                /*background-position-y: 40px;*/
                background-size: initial;
                background-repeat-x: repeat;
                background-repeat-y: no-repeat;
                background-attachment: initial;
                background-origin: initial;
                background-clip: initial;
                background-color: rgb(245, 245, 245);
            }

            .form-signin {
              max-width: 580px;
              padding: 10px;
              margin: 0 auto;
            }
            .form-signin .form-signin-heading {
              margin-bottom: 10px;
              text-align:center;
            }
            .form-signin .form-control {
              position: relative;
              font-size: 16px;
              height: auto;
              padding: 10px;
              -webkit-box-sizing: border-box;
                 -moz-box-sizing: border-box;
                      box-sizing: border-box;
            }
            .form-signin .form-control:focus {
              z-index: 2;
            }
            .form-signin input[type="text"], .form-signin input[type="password"] {
              margin-bottom: -1px;
              border-bottom-left-radius: 0;
              border-bottom-right-radius: 0;
            }
            .form-signin select[name="language"] {
              /*margin-bottom: 10px;*/
              border-top-left-radius: 0;
              border-top-right-radius: 0;
            }
            .form-center td{
                padding-left: 5px;
            }

            .redirect-button{
                max-width: 1000px;
                margin: 0 auto;
            }

            .redirect-button button{
                width: 150px;
                height: 60px;
                margin: 10px;
            }
        </style>
        <script>

            $(document).ready(function(){
                var cookieOption = { path: '/', expires: 1 };

                $("#buttonPanel").hide();

<%
                if(invalidSessionTimeout == null || invalidSessionTimeout.isEmpty()){
%>
                    $("#message").hide();
<%
                }else{
%>
                    $("#message").text("<%=invalidSessionTimeout%>");
                    $.cookie("COOKIE_USERID", null, cookieOption);
                    $.cookie("COOKIE_PASSWORD", null, cookieOption);
                    //$.cookie("COOKIE_LANGUAGE", null, cookieOption);
<%
                }
%>
                var cookieUserId   = $.cookie("COOKIE_USERID");
                var cookiePassword = $.cookie("COOKIE_PASSWORD");
                var cookieLanguage = $.cookie("COOKIE_LANGUAGE");

                if(cookieUserId != null)
                    $("#SSOForm input[name=userid]").val(cookieUserId);
                if(cookiePassword != null)
                    $("#SSOForm input[name=password]").val(cookiePassword);
                if(cookieLanguage != null)
                    $("#SSOForm select[name=language]").val(cookieLanguage);

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);

                
                //select language option
                $("#SSOForm select[name=language] option[value='<%=localeCode%>']").attr('selected', 'selected');

                $("#SSOForm select[name=language]").change(function(){
                    var language = $("option:selected", this).val();
                    var userId   = $("#SSOForm input[name=userid]").val();
                    var password = $("#SSOForm input[name=password]").val();

                    var date = new Date();
                    date.setTime(date.getTime() + (60 * 1000));
                    $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                    $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });

                    var languageDate = new Date();
                    languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                    $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                    changeLanguage(language);
                });

                $("#SSOForm input").keypress(function(e) {
                        if(e.which == 13 && $("#btn_checkLogin").is(":visible")) {
                            checkLogin();
                        }
                    });

                $("#btn_checkLogin").click(checkLogin);

                $("#SSOForm input[name=userid]").focus();
            });

            function checkLogin(){
                var userid = $("#SSOForm input[name=userid]").val();
                var password = $("#SSOForm input[name=password]").val();
                if(userid == null || userid == ""){//validate the input user id
                    $("#message").text("<%=bundle.getString("loginPage.caption.UserIdShouldNotBeEmpty")%>");
                    $("#message").show();
                }else if(password == null || password == ""){//validate the input password
                    $("#message").text("<%=bundle.getString("loginPage.caption.PasswordShouldNotBeEmpty")%>");
                    $("#message").show();
                }else{
                    $("#message").hide();
                    $("#message").text("");
                    /*
                    $("#SSOForm input[name=userid]").attr("readonly", true);
                    $("#SSOForm input[name=password]").attr("readonly", true);
                    $("#SSOForm select[name=language]").attr("readonly", true).attr("disabled", true);
                    */
                    //$("#btn_checkLogin").hide();


                    //renew the session
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "/SSO/SignIn"
                    });

                    var psmUserValid = false;
                    var wsmUserValid = false;

                    //check psm login
                    $.ajax({
                        async: false,
                        type: "GET",
                        url: "<%=SSOProperties.getProperty("PSM.USERVALIDATION.LINK")%>",
                        data: {
                            "username": userid,
                            "password": password
                        },
                        success: function(data){
                            if(data == true){
                                psmUserValid = true;
                            }
                        },
                        dataType: "json"
                    });

                    // check wsm login
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "<%=SSOProperties.getProperty("WSM.USERVALIDATION.LINK")%>",
                        data: {
                            "userid"    : userid,
                            "password"  : password,
                            "company"   : "AMG"
                        },
                        success: function(data){
                            if(data == true){
                                wsmUserValid = true;
                            }
                        },
                        dataType: "json"
                    });

                    if(psmUserValid && wsmUserValid){//both system user valid, redirect to button menu page
                        redirectToButtonMenu("Y", "Y");
                    }else if(psmUserValid){//psm user valid, redirect to button menu page
                        redirectToButtonMenu("Y", "N");
                    }else if(wsmUserValid){//wsm user valid, redirect to button menu page
                        redirectToButtonMenu("N", "Y");
                    }else{//both system user invalid, show login fail msg for both system.
                        var msg = "<%=bundle.getString("loginPage.caption.wsmLoginFail")%><br/><%=bundle.getString("loginPage.caption.psmLoginFail")%>";
                        $("#message").html(msg);
                        $("#message").show();
                    }

                }
            }


            //Change language function
            function changeLanguage(language){
                $.post("/SSO/changeLanguage.action", { "language": language },
                    function(data){
                        location.reload(true);
                });
            }

            // go to buttonMenu.jsp
            function redirectToButtonMenu(isPSMValid, isWSMValid){
                var form = $("#buttonMenuForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='buttonMenuForm' target='_self' action='/SSO/web/buttonMenu.jsp'></form>");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='isPSMValid' value='' />");
                    form.append("<input type='hidden' name='isWSMValid' value='' />");
                    $("body").append(form);
                }
                form.children("input[name=userid]").val($("#SSOForm input[name=userid]").val());
                form.children("input[name=password]").val($("#SSOForm input[name=password]").val());
                form.children("input[name=language]").val($("#SSOForm select[name=language]").val());
                form.children("input[name=isPSMValid]").val(isPSMValid);
                form.children("input[name=isWSMValid]").val(isWSMValid);
                form.submit();
            }
        </script>
    </head>
    <body>
        <div class="container">

            <form id="SSOForm" class="form-signin">
                <div class="jumbotron" style="opacity: 0.95;padding-top:5px;padding-bottom:5px">
                    <h2 class="form-signin-heading"><%=bundle.getString("loginPage.caption.singleSignOn")%></h2>
                    <table class="form-center">
                        <tr>
                            <td><%=bundle.getString("loginPage.caption.userId")%> &nbsp;</td>
                            <td>
                                <input type="text" name="userid" tabindex="1" autofocus="" placeholder="<%=bundle.getString("loginPage.caption.userId")%>" class="form-control">
                            </td>
                            <td rowspan="3" style="vertical-align:bottom">
                                <button type="button" id="btn_checkLogin" tabindex="4" class="btn btn-lg btn-primary"><%=bundle.getString("loginPage.button.signIn")%></button>
                            </td>
                        </tr>
                        <tr>
                            <td><%=bundle.getString("loginPage.caption.password")%> &nbsp;</td>
                            <td >
                                <input type="password" name="password" tabindex="2" placeholder="<%=bundle.getString("loginPage.caption.password")%>" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td><%=bundle.getString("loginPage.caption.language")%> &nbsp;</td>
                            <td>
                                <select name="language" tabindex="3" class="form-control">
                                    <option value="en__"  ><%=bundle.getString("loginPage.caption.english")%></option>
                                    <option value="zh_TW_"><%=bundle.getString("loginPage.caption.traditionalChinese")%></option>
                                    <option value="zh_CN_"><%=bundle.getString("loginPage.caption.simplifiedChinese")%></option>
                                    <%--<option value="ja_JP_"><%=bundle.getString("loginPage.caption.japanese")%></option>--%>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div id="message" class="alert alert-warning" style="padding-top:5px;padding-bottom:5px"></div>
                </div>
            </form>
        </div>
    </body>
</html>
