<%@ page import="java.util.ResourceBundle,java.util.Locale,java.util.HashMap,webcrm.bean.ui.LanguageElement" %>
<%@ page import="java.text.MessageFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="webcrm.util.SSOProperties" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%
    response.setContentType("text/html;charset=UTF-8");

    //UserInfo userInfo = (UserInfo)session.getAttribute("UserInfo");
    //WebUtil  webUtil  = (WebUtil)session.getAttribute("WebUtil");

    //ResourceBundle bundle = userInfo.getResourceBundle();

    // Get the parameter of company code
    //String companyCode = ConvertFormat.toString(request.getParameter("companyCode"));
    Locale locale = (Locale) session.getAttribute("language");

    ResourceBundle bundle = ResourceBundle.getBundle(SSOProperties.getProperty("RESOURCEBUNDLE_BASENAME"), locale);

    String localeCode = LanguageElement.getLocaleCode(locale);

    //HashMap<String, String> hm = (HashMap) session.getAttribute("errorAccess");
    HashMap<String, String> hm = null;

    //remove before error in this session
    session.removeAttribute("errorAccess");

    //String invalidLoginOrPassword = "";
    String invalidSessionTimeout = "";

    if (hm != null) {
//        if(hm.containsKey("error.message")){
//            invalidLoginOrPassword = hm.get("error.message");
//	}
        if (hm.containsKey("error.sessiontimeout")) {
            //invalidSessionTimeout = "<font color=\"#ff0000\">" + bundle.getString("loginPage.error.sessiontimeout") + "</font>";
            invalidSessionTimeout = bundle.getString("loginPage.error.sessiontimeout");
        }
    }

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <link href="/SSO/css/bootstrap.css" rel="stylesheet">
    <link href="/SSO/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/SSO/css/general/general.css"/>
    <script type="text/javascript" src="/SSO/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/SSO/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/SSO/js/cookie/jquery.cookie.js"></script>

    <style type="text/css">
            <%--body {
                margin-top: 0px;
                margin-right: auto;
                margin-bottom: 0px;
                margin-left: auto;
                padding-top: 0px;
                padding-right: 0px;
                padding-bottom: 0px;
                padding-left: 0px;
                height: 100%;
                vertical-align: top;
                background-color: #cccccc;
                text-align: left;
                background-repeat: no-repeat;
                background-position: center top;
            }--%>

            #mainContent
                {
                    width:1280px;
                    height:800px;
                    background-image: url("/SSO/img/P1.jpeg");
                    background-repeat: no-repeat;
                    background-position: center ;
                }

        </style>

    <script type="text/javascript">

        var authCodePath = "/SSO/forgetPasswordAuthCode.action";
        var submitPath   = "/SSO/forgetPassword.action";
        
        <%--function validateEmail($email) {
            if($email == null || $email == "")
                return false;
            
	    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    if( !emailReg.test( $email ) ) {
	        return false;
	    } else {
	        return true;
	    }
	}--%>

        $(document).ready(function(){
            //reset the form
            $(":input[type=text]").val("");
<%--
            $("#refreshAuthCode").bind('mouseover mouseout', function(e) {
                switch (e.type) {
                    case 'mouseover':
                        $(this).addClass('ui-state-hover');
                        break;
                    case 'mouseout':
                        $(this).removeClass('ui-state-hover');
                        break;
                }
            });
--%>
            $("#refreshAuthCode").mouseover(function(e) {$(this).addClass('ui-state-hover');})
                                .mouseout(function(e) {$(this).removeClass('ui-state-hover');});

            
            $("#backButton, #doneButton, #backButtonHome").click(function(){
                window.location.href = "/SSO";
                
            });

            $("#langPanel label[name=language]").click(function(){
                var language = $(this).attr("value");
                <%--var userId   = $("#langPanel input[name=userid]").val();
                var password = $("#langPanel input[name=password]").val();--%>

                var date = new Date();
                <%--date.setTime(date.getTime() + (60 * 1000));
                $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });--%>

                var languageDate = new Date();
                languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                changeLanguage(language);
            });

            //Change language function
            function changeLanguage(language){
                $.post("/SSO/changeLanguage.action", { "language": language },
                    function(data){
                        location.reload(true);
                });
            }

            <%--$("#nextStepButton").click(function(){
                $("#errorMessage").text("");
                
                var userid = $("#userid").val();
                var email  = $("#email").val();
                
                if(userid == null || userid == "" || email == null || email == ""){
                    $("#errorMessage").text("<%=bundle.getString("forgetPasswordPage.caption.userIdEmailBlankErrorMessage")%>");               
                    
                }else{
                    if(validateEmail($("#email").val())){
                        $("#step1").hide(100, function(){
                            $("#step2").show("slow");
                        });
                    }else{
                        $("#errorMessage").text("<%=bundle.getString("forgetPasswordPage.caption.emailErrorMessage")%>");
                    }
                }
                
            });
            $("#backStepButton").click(function(){
                $("#step2").hide(1500, function(){
                    $("#step1").show("slow");
                });
            });
            $("#retryButton").click(function(){
                //clean textfield
                $(":input[type=text]").val("");

                $("#step3").hide(1500, function(){
                    $("#step1").show("slow");
                });
            });--%>




            $("#refreshAuthCode").click(function(){
                $("#authCodeImage").attr("src", authCodePath + "?" + (new Date()).getTime());
            });

            $("#submitButton").click(function(){

                $("#errorMessage").text("");

                var userid = $("#userid").val();
                var authCode = $("#authCode").val();

                if(userid == null || userid == "" || authCode == null || authCode == ""){
                    $("#errorMessage").text("<%=bundle.getString("forgetPasswordPage.caption.userIdAuthCodeBlankErrorMessage")%>");
                    return false;
                }

                $("#errorMessage").text("");
                $("#submitButton").css("");
                $.post(submitPath, $("#forgetPasswordForm").serializeArray(),
                    function(json) {
                        if(json.authCode){

                            $("#step1").find("tr").not(":nth-child(1)").not(":nth-child(2)").not(":nth-child(7)").remove();
                            $("#submitButton").remove();
                            $("#step1").find("tr:nth-child(2)").html(json.message);
                            
                            if(json.isSuccess){
                                $("#doneButton").show();
                                $("#retryButton").hide();
                                $("#backButtonHome").hide();
                            }else{
                                $("#doneButton").hide();
                                $("#retryButton").show();
                                $("#backButtonHome").show();
                            }
                            
                            
                            //success/error message
                            $("#successMessage").html("<p>" + json.message + "</p>");
                            $("#step2").hide(1500, function(){
                                $("#step3").show("slow");
                            });

                            //


                        }else{
                            //error message
                            $("#authCode").val("");
                            $("#errorMessage").text(json.message);
                            $("#refreshAuthCode").trigger("click");
                        }
                        
                    },"json"
                );
            });
        });
    </script>
    <title><%=bundle.getString("forgetPasswordPage.caption.title")%></title>
</head>

<body style=" ">
    <center>
        <div id="mainContent">
    <%--<table style="width: 1280px; height: 800px; margin-right:8px; background-image: url('/SSO/img/P1.jpeg'); background-repeat: no-repeat;">--%>
    <table style="margin-right:8px;">
        <tr>
            <td rowspan="3" style="width: 960px;">
                &nbsp;
            </td>
            <td style="vertical-align: top; width: 320px; height: 260px; text-align: center;">
                <div id="langPanel" style="padding-top:10px; color: #acce22; font-size:24px">
                    <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.simplifiedChinese")%></label> |
                    <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.traditionalChinese")%></label> |
                    <label name="language" value="en__" style="cursor: pointer; color: #acce22"><%=bundle.getString("loginPage.caption.english")%></label>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" style="color: rgb(0, 0, 153);">
                    <form id="forgetPasswordForm" method="post" action="#">
                    <table id="step1" width="300px;">
                        <tr align="center">
                            <td colspan="5" style="font-size:30px"><%=bundle.getString("forgetPasswordPage.caption.windowsTitle")%><hr/></td>
                         </tr>
                         <tr align="left">
                             <td colspan="5"><%=bundle.getString("forgetPasswordPage.caption.enterUserIdEmailMessage")%></td>
                         </tr>
                         <tr align="left">
                             <td colspan="5">&nbsp;</td>
                         </tr>
                         <tr align="left">
                            <td>&nbsp;</td>
                            <td style="width:60px"><%=bundle.getString("forgetPasswordPage.caption.userid")%></td>
                            <td style="width:10px">:</td>
                            <td style="width:100px">
                                <input type="text" class="text" tabindex="1" id="userid" name="userid" maxlength="12" style="width:150px"/>
                                <%--<input type="hidden" id="companyCode" name="companyCode" value="<%=companyCode%>"/>--%>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--<tr align="left">
                            <td>&nbsp;</td>
                            <td><%=bundle.getString("forgetPasswordPage.caption.email")%></td>
                            <td>:</td>
                            <td><input type="text" class="text" tabindex="2" id="email" name="email" style="width:150px"/></td>
                            <td>&nbsp;</td>
                        </tr>--%>
                        <tr align="left">
                            <td style="width:30px">&nbsp;</td>
                            <td style="width:100px"><%=bundle.getString("forgetPasswordPage.caption.authCode")%></td>
                            <td>:</td>
                            <td><input type="text" class="text" tabindex="3" id="authCode" name="authCode" style="width:150px"/></td>
                            <td style="width:30px">&nbsp;</td>
                        </tr>
                        <tr align="right">
                            <td style="width:30px">&nbsp;</td>
                            <td colspan="3"><img id="authCodeImage" style="width:100%; height:60px;" src="/SSO/forgetPasswordAuthCode.action" /></td>
                            <td style="vertical-align:bottom"><button type="button" id="refreshAuthCode" class="ui-state-default ui-corner-all" style="width:22px;"><span class="ui-icon ui-icon-refresh"/></button></td>
                        </tr>
                        <tr align="left">
                             <td colspan="5" align="center">
                                <br>
                                <input id="submitButton" type="button" tabindex="4" style="height:30px; padding-top:0px; padding-bottom:0px;" class="btn btn-lg btn-primary" value="<%=bundle.getString("forgetPasswordPage.caption.submitButton")%>"/>
                                <input id="backButton" type="button" tabindex="5" style="height:30px; padding-top:0px; padding-bottom:0px;" class="btn btn-lg btn-primary" value="<%=bundle.getString("forgetPasswordPage.caption.backButton")%>"/>
                             </td>
                        </tr>
                   </table>
                   <%--<table id="step2" style="width:90%;display:none">
                        <tr align="center">
                            <td colspan="5" style="font-size:30px"><%=bundle.getString("forgetPasswordPage.caption.windowsTitle")%><hr/></td>
                         </tr>
                        <tr align="left">
                            <td colspan="5"><%=bundle.getString("forgetPasswordPage.caption.enterCharactersMessage")%></td>
                        </tr>
                        <tr align="left">
                            <td style="width:30px">&nbsp;</td>
                            <td colspan="3"><img id="authCodeImage" style="width:300px; height:80px;" src="<%=webUtil.getContextPath()%>forgetPasswordAuthCode.action" /></td>
                            <td style="vertical-align:bottom"><button type="button" id="refreshAuthCode" class="ui-state-default ui-corner-all" style="width:22px;"><span class="ui-icon ui-icon-refresh"/></button></td>
                        </tr>
                        <tr align="left">
                            <td style="width:30px">&nbsp;</td>
                            <td style="width:100px"><%=bundle.getString("forgetPasswordPage.caption.authCode")%></td>
                            <td>:</td>
                            <td><input type="text" class="text" tabindex="3" id="authCode" name="authCode" style="width:150px"/></td>
                            <td style="width:30px">&nbsp;</td>
                        </tr>
                        <tr align="left">
                             <td colspan="5" align="center">
                                <br>
                                <input id="submitButton"   type="button" tabindex="4" class="general_button" value="<%=bundle.getString("forgetPasswordPage.caption.submitButton")%>"/>
                                <input id="backStepButton" type="button" tabindex="5" class="general_button" value="<%=bundle.getString("forgetPasswordPage.caption.backButton")%>"/>
                             </td>
                         </tr>
                   </table>--%>
                   <%--<table id="step3" style="width:90%;display:none">
                        <tr align="center">
                            <td colspan="5" style="font-size:30px"><%=bundle.getString("forgetPasswordPage.caption.windowsTitle")%><hr/></td>
                         </tr>
                        <tr align="left">
                            <td colspan="5" id="successMessage"></td>
                        </tr>
                        <tr align="left">
                             <td colspan="5" align="center">
                                <br>
                                <input id="doneButton"  type="button" tabindex="5" class="general_button" value="<%=bundle.getString("forgetPasswordPage.caption.doneButton")%>"/>
                                <input id="retryButton" type="button" tabindex="5" class="general_button" value="<%=bundle.getString("forgetPasswordPage.caption.retryButton")%>"/>
                                <input id="backButtonHome"  type="button" tabindex="6" class="general_button" value="<%=bundle.getString("forgetPasswordPage.caption.backButton")%>"/>
                             </td>
                         </tr>
                   </table>
                   <br/>--%>
              </form>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;">
                <p id="errorMessage" style="color:#ff0000; padding-left: 8px;"></p>
            </td>
        </tr>
    </table>
            </div>
    </center>
</body>
</html>
